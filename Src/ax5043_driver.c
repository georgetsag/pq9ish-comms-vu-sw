/*
 *  A PQ9 based Weather Station
 *
 *  Copyright (C) 2017 Libre Space Foundation
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ax5043_driver.h"
#include <stm32l4xx_hal.h>
#include "main.h"
#include "utils.h"
#include <string.h>

extern SPI_HandleTypeDef hspi2;

/**
 * Enables/disables the SEL pin of an SPI device
 * @param dev the SPI device
 * @param enable set 1 to enable, 0 to disable
 * @return 0 on success or appropriate negative error code
 */
static int
spi_sel_dev(spi_dev_t dev, bool enable)
{
	/* TODO: Disable all other existing SPI devices*/
	switch (dev) {
		case SPI_DEV_AX5043:
			HAL_GPIO_WritePin(AX5043_SEL_GPIO_Port,
			                  AX5043_SEL_Pin, (~enable & 0x1));
			break;
		default:
			/* FIXME: Return meaningful error code */
			return -1;
	}
	return 0;
}

int
ax5043_drv_dev_select(bool enable)
{
	return spi_sel_dev(SPI_DEV_AX5043, enable);
}

int
ax5043_drv_spi_read(uint8_t *rx, uint8_t *tx, uint32_t len)
{
	return HAL_SPI_TransmitReceive(&hspi2, tx, rx, len, SPI_TIMEOUT);
}

int
ax5043_drv_spi_write(uint8_t *rx, uint8_t *tx, uint32_t len)
{
	return HAL_SPI_Transmit(&hspi2, tx, len, SPI_TIMEOUT);
}
