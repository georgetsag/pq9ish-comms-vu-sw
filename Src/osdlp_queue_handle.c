/*
 *  Implementation of OSDLP queue API
 *
 *  Copyright (C) 2020 Libre Space Foundation (https://libre.space)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "osdlp_queue_handle.h"
#include "osdlp_config.h"

int
initialize_osdlp()
{
	tm_master_channel_count = 0;
	int ret;
	for (int i = 0 ; i < OSDLP_TC_VCS; i++) {
		ret = init_queue(&rx_queues[i],
		                 OSDLP_MAX_TC_PACKET_LENGTH,
		                 OSDLP_TC_ITEMS_PER_QUEUE,
		                 tc_mem_space[i]);
		if (rx_queues[i].is_init != NO_ERROR) {
			return -1;
		}
		prepare_farm(&tc_configs[i].cop_cfg.farm, FARM_STATE_OPEN, tc_window_width[i]);
		ret = tc_init(&tc_configs[i], OSDLP_SCID, OSDLP_MAX_TC_PACKET_LENGTH,
		              OSDLP_MAX_TC_FRAME_SIZE, OSDLP_TC_ITEMS_PER_QUEUE, i, 0, tc_crc_on[i],
		              tc_segmentation_per_vcid[i], 0, 0, tc_util_buffer[i], tc_configs[i].cop_cfg);
		if (ret == -1) {
			return -1;
		}
	}
	for (int i = 0 ; i < OSDLP_TM_VCS; i++) {
		ret = init_queue(&tx_queues[i],
		                 OSDLP_TM_FRAME_SIZE,
		                 OSDLP_TM_ITEMS_PER_QUEUE,
		                 tm_mem_space[i]);
		if (tx_queues[i].is_init != NO_ERROR) {
			return -1;
		}
		ret = tm_init(&tm_configs[i], OSDLP_SCID,
		              &tm_master_channel_count, i,
		              tm_ocf_flag[i], tm_sec_hdr_on[i],
		              tm_sync_flag[i], tm_sec_hdr_len[i],
		              NULL, 0, tm_crc_on[i],
		              OSDLP_TM_FRAME_SIZE, OSDLP_TM_FRAME_SIZE,
		              OSDLP_TM_VCS,
		              OSDLP_TM_ITEMS_PER_QUEUE,
		              tm_stuffing_state[i],
		              tm_util_buffer[i]);
		if (ret == -1) {
			return -1;
		}
	}
	osdlp_state.tc_configs = tc_configs;
	osdlp_state.tm_configs = tm_configs;
	osdlp_state.rx_queues  = rx_queues;
	osdlp_state.tx_queues  = tx_queues;
	return;
}

struct osdlp_state_t *
get_osdlp_state()
{
	return &osdlp_state;
}

bool
tc_rx_queue_full(uint16_t vcid)
{
	return rx_queues[vcid].inqueue ==
	       rx_queues[vcid].capacity ? true : false;
}

int
tc_rx_queue_enqueue(uint8_t *buffer, uint16_t vcid)
{
	int ret = enqueue(&rx_queues[vcid], buffer);
	return ret;
}

int
tc_rx_queue_enqueue_now(uint8_t *buffer, uint8_t vcid)
{
	int ret = enqueue(&rx_queues[vcid], buffer);
	if (ret < 0) {
		if (tc_rx_queue_full(vcid)) {
			ret = enqueue_now(&rx_queues[vcid], buffer);
			return 0;
		}
	} else {
		return 0;
	}
	return -1;
}

int
tc_get_rx_config(struct tc_transfer_frame **tf, uint16_t vcid)
{
	*tf = &tc_configs[vcid];
	if (tf != NULL) {
		return 0;
	} else {
		return -1;
	}
}

bool
tm_tx_queue_empty(uint8_t vcid)
{
	return (tx_queues[vcid].inqueue == 0);
}

int
tm_tx_queue_back(uint8_t **pkt, uint8_t vcid)
{
	int ret = back(&tx_queues[vcid], *pkt);
	if (ret < 0) {
		return -1;
	} else {
		return 0;
	}
}

int
tm_tx_queue_enqueue(uint8_t *pkt, uint8_t vcid)
{
	int ret = enqueue(&tx_queues[vcid], pkt);
	return ret;
}

void
tm_tx_commit_back(uint8_t vcid)
{
	return;
}

int
tm_get_packet_len(uint16_t *length, uint8_t *pkt, uint16_t mem_len)
{
	if (mem_len >= 5) {
		if (((pkt[3] << 8) | pkt[4]) <= OSDLP_MAX_TM_PACKET_LENGTH) {
			*length = ((pkt[3] << 8) | pkt[4]);
			return 0;
		} else {
			return -1;
		}
	} else {
		return -1;
	}
}
