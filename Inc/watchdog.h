
#ifndef WATCHDOG_H_
#define WATCHDOG_H_

#include <stdint.h>
#include "stm32l4xx_hal.h"
#include "cmsis_os.h"

struct watchdog {
	uint8_t               subsystems_num;
	uint32_t              subsystems;
	uint32_t              subsystems_mask;
	uint8_t               registered;
	osMutexId             mtx;
	IWDG_HandleTypeDef    *hiwdg;
};

int
watchdog_init(struct watchdog *w, IWDG_HandleTypeDef *hiwdg,
              osMutexId mtx, uint8_t n);

int
watchdog_register(struct watchdog *w, uint8_t *id);

int
watchdog_reset(struct watchdog *w);

int
watchdog_reset_subsystem(struct watchdog *w, uint8_t id);


#endif /* WATCHDOG_H_ */
