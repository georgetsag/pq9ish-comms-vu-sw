#ifndef TEST_OSDLP_CONFIG_H_
#define TEST_OSDLP_CONFIG_H_

#include <Drivers/OSDLP/include/osdlp.h>
#include "conf.h"

/******************************************************************************
 ****************************** COP Params **********************************
 *****************************************************************************/

tc_seg_hdr_t tc_segmentation_per_vcid[OSDLP_TC_VCS] = {TC_SEG_HDR_NOTPRESENT, TC_SEG_HDR_NOTPRESENT,
                                                       TC_SEG_HDR_NOTPRESENT, TC_SEG_HDR_NOTPRESENT, TC_SEG_HDR_NOTPRESENT, TC_SEG_HDR_NOTPRESENT,
                                                       TC_SEG_HDR_NOTPRESENT, TC_SEG_HDR_NOTPRESENT, TC_SEG_HDR_NOTPRESENT, TC_SEG_HDR_NOTPRESENT
                                                      };

tc_crc_flag_t tc_crc_on[OSDLP_TC_VCS] = {TC_CRC_PRESENT, TC_CRC_PRESENT, TC_CRC_PRESENT,
                                         TC_CRC_PRESENT, TC_CRC_PRESENT, TC_CRC_PRESENT, TC_CRC_PRESENT,
                                         TC_CRC_PRESENT, TC_CRC_PRESENT, TC_CRC_PRESENT
                                        };

bool tc_window_width[OSDLP_TC_VCS] = {10, 10, 10,
                                      10, 10, 10, 10, 10, 10, 10
                                     };

/******************************************************************************
 ****************************** TM Params **********************************
 *****************************************************************************/

tm_ocf_flag_t tm_ocf_flag[OSDLP_TM_VCS] = { TM_OCF_NOTPRESENT,
                                            TM_OCF_NOTPRESENT,
                                            TM_OCF_NOTPRESENT
                                          };

uint32_t tm_ocf[OSDLP_TM_VCS] = {0, 0, 0};

tm_crc_flag_t tm_crc_on[OSDLP_TM_VCS] = {TM_CRC_PRESENT,
                                         TM_CRC_PRESENT,
                                         TM_CRC_PRESENT
                                        };

tm_sec_hdr_flag_t tm_sec_hdr_on[OSDLP_TM_VCS] = {TM_SEC_HDR_NOTPRESENT,
                                                 TM_SEC_HDR_NOTPRESENT,
                                                 TM_SEC_HDR_NOTPRESENT
                                                };

uint8_t tm_sec_hdr_len[OSDLP_TM_VCS] = {0, 0, 0};

tm_sync_flag_t tm_sync_flag[OSDLP_TM_VCS] = {TYPE_VCA_SDU,
                                             TYPE_VCA_SDU,
                                             TYPE_VCA_SDU
                                            };

tm_stuff_state_t tm_stuffing_state[OSDLP_TM_VCS] = {TM_STUFFING_OFF,
                                                    TM_STUFFING_OFF,
                                                    TM_STUFFING_OFF
                                                   };

#endif /* TEST_OSDLP_CONFIG_H_ */
